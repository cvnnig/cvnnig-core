import torch
import numpy as np

def parse_bbox_xywh(bbox_string: str, boundary_elements="[]", separator_elemment=",") -> (float, float, float, float):
    """
    Reconstitute bboxes by parusing the string and return the xywh in floats.
    """
    # remove boundary elements
    for element in boundary_elements:
        bbox_string = bbox_string.replace(element, '')

    # Parse them into four parts list using the separator
    bbox_string = bbox_string.split(separator_elemment)

    # Convert it into four parts: 
    # You SURE it is min/min/max/mx?
    bbox_xmin = float(bbox_string[0])
    bbox_ymin = float(bbox_string[1])
    bbox_width = float(bbox_string[2])
    bbox_height = float(bbox_string[3])

    return bbox_xmin, bbox_ymin, bbox_width, bbox_height


def xyxy2xywh(x):
    """
    Convert nx4 boxes from [x1, y1, x2, y2] to [x, y, w, h] where xy1=top-left, xy2=bottom-right
    Stolen from YOLOV5 Utility Script.
    :param x:
    :return:
    """

    y = torch.zeros_like(x) if isinstance(x, torch.Tensor) else np.zeros_like(x)
    y[:, 0] = (x[:, 0] + x[:, 2]) / 2  # x center
    y[:, 1] = (x[:, 1] + x[:, 3]) / 2  # y center
    y[:, 2] = x[:, 2] - x[:, 0]  # width
    y[:, 3] = x[:, 3] - x[:, 1]  # height
    return y


def xywh2xyxy(x):
    # Convert nx4 boxes from [x, y, w, h] to [x1, y1, x2, y2] where xy1=top-left, xy2=bottom-right
    y = torch.zeros_like(x) if isinstance(x, torch.Tensor) else np.zeros_like(x)
    y[:, 0] = x[:, 0] - x[:, 2] / 2  # top left x
    y[:, 1] = x[:, 1] - x[:, 3] / 2  # top left y
    y[:, 2] = x[:, 0] + x[:, 2] / 2  # bottom right x
    y[:, 3] = x[:, 1] + x[:, 3] / 2  # bottom right y
    return y
