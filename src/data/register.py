from detectron2.data import DatasetCatalog, MetadataCatalog
from src.models_cvnnig.iMaterialist2020.detectron2_model_train import load_dataset_into_dataframes, convert_to_datadict


def register_datadict_for_detectron2(datadict_input, label_dataset:str = "sample_fashion_train"):
    """
    Register the data type with the Catalog function from Detectron2 code base.
    fixme: currently hard coded as sample_fashion_train sample_fashion_test
    """
    _, _, df_categories = load_dataset_into_dataframes()
    # Register the train and test and set metadata

    DatasetCatalog.register(label_dataset, lambda d=datadict_input: convert_to_datadict(d))
    MetadataCatalog.get(label_dataset).set(thing_classes=list(df_categories.name))