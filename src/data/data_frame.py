from pathlib import Path
import numpy as np
import pandas as pd
from typing import Tuple
from PythonUtils.PULog import logger
from src.envpath import AllPaths


def data_check_info(train_df: pd.DataFrame, field_id: str = "image_id"):
    """
    This function checks the panda data frames for basic properities.
    :param train_df:
    :return:
    """
    # Quick head tail check.
    logger.info(f"Data frame head check: "
                f"{train_df.head}")

    logger.info(f"Data frame tail check: "
                f"{train_df.tail}")

    # Shape should be 147793 vs 5
    logger.info(f"Data frame has shape of {train_df.shape}")

    # Uniques, should be 3373
    logger.info(f"Data frame has unique shape of {train_df[field_id].unique().shape}")

    # Entry check.
    logger.info(f"Data frame info: {train_df.info()}")


def load_train_csv(file_name="train.csv") -> pd.DataFrame:
    """
    Load the labeling data for decoding purpose
    :return:
    """

    df_train = pd.read_csv(AllPaths.raw / file_name)
    return df_train

def load_train_processed_csv(sample=False) -> pd.DataFrame:
    """
    Load the PROCESSED labeling data for decoding purpose
    :return:
    """

    df_train_processed = pd.read_csv(AllPaths.interim / "train_processed.csv")
    if sample:
        return df_train_processed[:1000]
    else: 
        return df_train_processed


def load_random_partial_subsets(list_full: list, threshold: float=0.95) -> list:
    """
    Given a list, randomly retrieve a sub portion of the list.
    :param list_full:
    :param threshold:
    :return:
    """
    logger.warning("Must ensure seed consistency.")

    train_files = set(
        np.random.choice(
            list_full,
            int(len(list_full) * threshold),
            replace=False
        )
    )
    return list(train_files)


def split_dataframe_via_list(df_complete: pd.DataFrame, property: str, list_train: list) -> Tuple[pd.DataFrame, pd.DataFrame]:
    """
    Using a specific list, split the datasets into multiple chunks.
    :param df_complete:
    :param property:
    :param list_train:
    :return:
    """
    # Force list conversion
    if type(list_train) is set:
        list_train = list(list_train)

    # Train from the trained list.
    df_train_partial = df_complete[df_complete[property].isin(list_train)]
    logger.info(f"Training portion (1st) has a shape of: {df_train_partial.shape}")

    # Val from not on the trained list, which are the left behind entries.
    df_val_partial = df_complete[~df_complete[property].isin(list_train)]
    logger.info(f"Validation portion (2nd) has a shape of: {df_val_partial.shape}")
    return df_train_partial, df_val_partial


def get_label_classes(df_complete: pd.DataFrame, property: str) -> list:
    """
    Get a dataframe property and return unique elements as class
    :param df_complete:
    :param property:
    :return:
    """
    classes = df_complete[property].unique().tolist()
    logger.info(f"Full datasets contain {len(classes)} classes:")
    logger.info(classes)
    return classes


def save_to_feather(df_train, df_val, threshold, sample):
    """
    Get a dataframe and save to feather format
    :param df_train:
    :param df_val:
    """
    if threshold == 1: 
        df_size = '_full'
    elif sample:
        df_size = '_sample'
    else:
        df_size = ''

    df_train.reset_index().to_feather(AllPaths.processed / f'wheat_train{df_size}')
    logger.info(f"Saving training dataframe to {AllPaths.processed}")
    if threshold < 1:
        df_val.reset_index().to_feather(AllPaths.processed / f'wheat_val{df_size}')
        logger.info(f"Saving validation dataframe to {AllPaths.processed}")




