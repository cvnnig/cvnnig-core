# A script to delete all contest related the data and prepare for next competition.
from datetime import datetime
from pathlib import Path
import os

import click

@click.command()
def clean_up_operation():
	contest_end = datetime.now().isoformat().replace(":", "")
	print("""This script will clean up all the data that exist under 'data' folder, including everything in the
		external, interim, processed, raw etc.
		These data are NOT in the version control and will result in permanent loss once wiped.
		YOU HAVE BEEN WARNED!
		Reply YES to proceed.""")

	if input().upper() != "YES":
		print("Aborted")
		return

	print("""Do you want to enter a name for this past competition? 
		If not, leave empty and timstamp will be used instead.
	""")
	post_fix = input()

	if post_fix == '':
		post_fix = f"ContestEnding{contest_end}"

	path_project = Path(__file__).parents[2]

	path_results = path_project / "results"
	if path_results.exists():
		os.rename(path_results, f"{path_results}_{post_fix}")
		os.mkdir(path_results)

	path_data = path_project / "data"
	if path_data.exists():
		os.rename(path_data, f"{path_data}_{post_fix}")
		os.mkdir(path_data)

	path_notebooks = path_project / "notebooks"
	if path_notebooks.exists():
		os.rename(path_notebooks, f"{path_notebooks}_{post_fix}")
		os.mkdir(path_notebooks)

	print("Done")

if __name__ == "__main__":
	clean_up_operation()
