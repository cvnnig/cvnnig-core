import numpy as np
from numpy import ndarray


def content_crop(img: ndarray, white_background: bool):
    """
    Center the content, removed
    https://www.kaggle.com/iafoss/image-preprocessing-128x128

    :param img: grapheme image matrix
    :param white_background: whether the image
    :return: cropped image matrix
    """
    # Remove the surrounding 5 pixels
    img = img[5:-5, 5:-5]
    if white_background:
        y_list, x_list = np.where(img < 235)
    else:
        y_list, x_list = np.where(img > 80)

    # get xy min max
    xmin, xmax = np.min(x_list), np.max(x_list)
    ymin, ymax = np.min(y_list), np.max(y_list)

    # Manually set the baseline low and high for x&y
    xmin = xmin - 13 if (xmin > 13) else 0
    ymin = ymin - 10 if (ymin > 10) else 0
    xmax = xmax + 13 if (xmax < 223) else 236
    ymax = ymax + 10 if (ymax < 127) else 137

    # Reposition the images
    img = img[ymin:ymax, xmin:xmax]

    return img


def pad_to_square(img: ndarray, white_background: bool):
    ly, lx = img.shape

    l = max(lx, ly) + 16
    if white_background:
        constant_pad = 255
    else:
        constant_pad = 0
    img = np.pad(img, [((l - ly) // 2,), ((l - lx) // 2,)], mode='constant', constant_values=constant_pad)
    return img


