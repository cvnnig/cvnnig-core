from PythonUtils.rle_encoding import RLE_encoding
from PythonUtils.rle_decoding import RLE_decoding
import numpy as np
from typing import List
from PIL import Image
import numpy

def test_encoding():
	image = numpy.asarray((Image.open(r'C:\temp\00000663ed1ff0c4e0132b9b9ac53f6e_annotated.png')))

	a = image.reshape()
	print(a[6068159])
	b = image.flatten(order="F")
	print(b[6068159])
	c = image.flatten(order="A")
	print(c[6068159])
	d = image.flatten(order="K")
	print(d[6068159])

	#Result = RLE_encoding(image, mask_value=255)
	#print(Result.get())
	pass