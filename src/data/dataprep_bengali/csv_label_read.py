from pathlib import Path
from PythonUtils.rle_encoding import RLE_encoding
from dotenv import load_dotenv, find_dotenv
load_dotenv(find_dotenv())
import numpy as np
from typing import List
import csv
import os
from pathlib import Path
from log import logger
import pandas as pd
from src.data.data_frame import load_train_csv


def pandaread_image_labels():
	"""
	Direct panda read the 6GB CSV. Took less than 2s on the NVME
	:param path_csv: Path to the gigantic gigabyte image RLE CSVs
	:return:
	"""
	return load_train_csv()


def encoding_files():
	"""
	todo: unfinished test getting RLE encoding function from all excel into iamges.
	:return:
	"""
	import csv
	PATH_DATA_RAW = os.getenv("PATH_DATA_RAW")

	with open(Path(PATH_DATA_RAW) / 'train.csv', newline='') as csvfile:
		labelreader = csv.reader(csvfile)

		# Skip header row
		labelreader.next()
		for row in labelreader:
			logger.info(row.split(","))


