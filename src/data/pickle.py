import pickle
from src.envpath import AllPaths

def load_data_train(file_pickle: str = "train_data.p"):
    """
    Load the training data from the interim path
    :param file_pickle: name of the pickle file, default to train_data.p
    :return:
    """
    with open(AllPaths.interim / file_pickle, 'rb') as pickle_file:
        data_train = pickle.load(pickle_file)
        return data_train


def load_data_val(file_pickle: str = "val_data.p"):
    """
    Load the validation data from the interim path
    :param file_pickle: name of the pickle file, default to val_data.p
    :return:
    """
    with open(AllPaths.interim / file_pickle, 'rb') as pickle_file:
        data_val = pickle.load(pickle_file)
        return data_val
