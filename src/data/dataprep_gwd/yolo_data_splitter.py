from pathlib import Path
import math
from glob import glob
import random
import shutil


def move_images_labels(path_images: Path or str,
                       path_labels: Path or str,
                       path_out: Path or str,
                       proportion: float = 0.2, ):

    path_images = Path(path_images)
    path_labels = Path(path_labels)
    path_out = Path(path_out)

    # Generate a list of all stems in the path images.
    set_all_image_stems = set(map(lambda file_path: Path(file_path).stem,
                                  glob(str(path_images / "*.jpg"))
                                  ))
    set_all_label_stems = set(map(lambda file_path: Path(file_path).stem,
                                  glob(str(path_labels / "*.txt"))
                                  ))

    set_available_stems = set.intersection(set_all_image_stems, set_all_label_stems)

    # Sample the proportion of example from the eligible images.
    list_selected_stems = random.sample(set_available_stems,
                                        math.floor(int(len(set_available_stems)) * proportion)
                                        )

    for stem in list_selected_stems:
        shutil.move(path_images / f"{stem}.jpg", path_out / f"{stem}.jpg")
        shutil.move(path_labels / f"{stem}.txt", path_out / f"{stem}.txt")

if __name__ == '__main__':
    move_images_labels(
        "/home/dyt811/Git/cvnnig/data_gwd_yolo/images/train",
        "/home/dyt811/Git/cvnnig/data_gwd_yolo/labels/train",
        "/home/dyt811/Git/cvnnig/data_gwd_yolo"
    )