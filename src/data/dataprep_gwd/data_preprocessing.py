from tqdm import tqdm
from typing import List
import pandas as pd

from src.envpath import AllPaths
from src.data.bbox import parse_bbox_xywh
from src.data.data_frame import load_train_csv

def preprocess_to_csv(save=True, name="train_processed.csv"):

    # Data preprocessing check

    # Read the csv train data, which is always in the raw folder called train.csv
    df_train_raw = load_train_csv()

    # List of data dict
    dataset: List[dict] = []

    # Use TQDM to iterate through the data frame rows
    for index, mask_row in tqdm(
            df_train_raw.iterrows(),
            total=df_train_raw.shape[0]
    ):
        # Get the image name, MIGHT need full path here.
        image_name = f"{mask_row['image_id']}.jpg"

        bbox_xmin, bbox_ymin, bbox_width, bbox_height = parse_bbox_xywh(mask_row['bbox'])

        # Instantiate a dict?
        data: dict = {}

        width = mask_row['width']
        height = mask_row['height']

        data['file_name'] = image_name
        data['width'] = width
        data['height'] = height

        data["x_min"] = bbox_xmin
        data["y_min"] = bbox_ymin
        data["x_max"] = bbox_xmin + bbox_width
        data["y_max"] = bbox_ymin + bbox_height

        data['class_name'] = 'wheat'

        # Append to the list of Dict
        dataset.append(data)

    # Convert to dataframe.
    df_train_processed = pd.DataFrame(dataset)

    if save:
        # Save to CSV.
        df_train_processed.to_csv(AllPaths.interim / name, header=True, index=None)

    # Return the processed dataframe.
    return df_train_processed

if __name__ == "__main__":
    preprocess_to_csv()
