
from pathlib import Path
from src.envpath import AllPaths

from src.data.dataprep_gwd.create_data_dicts import dataframe_to_d2dicts

from detectron2.data import DatasetCatalog, MetadataCatalog

path_train, path_test = AllPaths.train_data, AllPaths.test_data


def register_datadict(df, train=True, submission=False):
    """
    Register the data type with the Catalog function from Detectron2 code base.
    """
    if train and not submission: # training
        label, image_dir = 'wheat_train', path_train
    elif not train and not submission: # validation
        label, image_dir = 'wheat_test', path_train
    elif submission: # test
        label, image_dir = 'wheat_test', path_test

    # Register the train and test and set metadata
    DatasetCatalog.register(label, lambda d=df: dataframe_to_d2dicts(d, image_dir))
    MetadataCatalog.get(label).set(thing_classes=['wheat'])

    return MetadataCatalog.get(label), label

