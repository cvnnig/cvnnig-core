import itertools
from tqdm import tqdm
from typing import List
from detectron2.structures import BoxMode
from src.envpath import AllPaths
import pandas as pd
    
def dataframe_to_d2dicts(df_train_processed: pd.DataFrame, img_folder: str) -> List[dict]:
    """
    Convert the given data frames to detectron 2 data dicts to be eventually registered.
    This will be competition specific as no competition used the SAME field names.

    :param df_train_processed:
    :param classes:
    :return:
    """
    grps = df_train_processed['file_name'].unique().tolist()
    df_group = df_train_processed.groupby('file_name')
    dataset_dicts = []

    for image_id, img_name in enumerate(tqdm(grps)):

        group = df_group.get_group(img_name)
        # Intialized the record dict.
        image_record = {}

        # Get image data frame. This is only for a SINGLE image.
        #df_image = df_train_processed[df_train_processed.file_name == img_name]

        file_path = str(img_folder / img_name)

        image_record["file_name"] = file_path
        image_record["image_id"] = image_id
        image_record["height"] = int(group.iloc[0].height)
        image_record["width"] = int(group.iloc[0].width)

        list_object_annotations = []

        # Go through each row (each is a different bounding box), convert the box to poly.
        for row in group.itertuples():
            xmin = int(row.x_min)
            ymin = int(row.y_min)
            xmax = int(row.x_max)
            ymax = int(row.y_max)

            poly = [
                (xmin, ymin), (xmax, ymin),
                (xmax, ymax), (xmin, ymax)
            ]
            poly = list(itertools.chain.from_iterable(poly))

            # This is the format for the Detectron2 annotation.
            obj = {
                "bbox": [xmin, ymin, xmax, ymax],
                "bbox_mode": BoxMode.XYXY_ABS,
                "segmentation": [poly],
                "category_id": 0,
                "iscrowd": 0
            }
            list_object_annotations.append(obj)

        image_record["annotations"] = list_object_annotations

        dataset_dicts.append(image_record)
    return dataset_dicts