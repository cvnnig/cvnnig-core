import csv
import pandas as pd
from tqdm import tqdm
from src.data.data_frame import load_train_csv
from typing import List
from src.envpath import AllPaths

def getnerate_yolo_labels_set():
    # Load CSV
    df_train = load_train_csv()

    # Parse Sort by Image_IDs
    list_image_files = df_train['image_id'].unique().tolist()
    df_group = df_train.groupby('image_id')

    for image_index, image_file_name in enumerate(tqdm(list_image_files)):
        write_image_yolo_labels(df_group, image_file_name)


def write_image_yolo_labels(df_group, image_file_name: str):
    bbox_group = df_group.get_group(image_file_name)
    # Label format: https://github.com/AlexeyAB/Yolo_mark/issues/60
    image_record = []
    label_class = 0
    # Get image data frame. This is only for a SINGLE image.
    # Loop through each bbox and construct the CSV rows
    for _, bbox_row_entry in bbox_group.iterrows():
        bbox_record = generate_yolo_bbox_record(bbox_row_entry)
        image_record.append(bbox_record)
    write_image_record_csv(image_record, image_file_name)


def write_image_record_csv(image_record: List[dict], image_file_name):
    # Write to CSV:
    image_record_keys = image_record[0].keys()

    label = AllPaths.interim / f"{image_file_name}.txt"

    with open(f'{str(label)}', 'w') as output_file:
        dict_writer = csv.DictWriter(output_file, image_record_keys, delimiter=" ", quoting=csv.QUOTE_NONE)
        # dict_writer.writeheader() # Yolo does not require label header.
        dict_writer.writerows(image_record)


def generate_yolo_bbox_record(bbox_row_entry, label_class=0):
    # Intialized the record dict.
    bbox_record = {}
    #print(bbox_row_entry.bbox[1:-1:1].split(", "))
    try:
        (xmin, ymin, bbox_width, bbox_height) = tuple(map(float, tuple(bbox_row_entry.bbox[1:-1:1].split(","))))
    except:
        print()

    image_width = float(bbox_row_entry.height)
    image_height = float(bbox_row_entry.width)

    bbox_record["class"] = label_class  # default to zero for single classificaiton

    # center X coordinate of rectangle label mask
    bbox_record["x%"] = float(
        (xmin + 0.5 * bbox_width) / image_width
    )

    # center Y coordinate of rectangle label mask
    bbox_record["y%"] = float(
        (ymin + 0.5 * bbox_height) / image_height
    )

    # width in proportion
    bbox_record["width%"] = float(
        bbox_width / image_width
    )

    # height in proportion
    bbox_record["height%"] = float(
        bbox_height / image_height
    )

    return bbox_record

if __name__=="__main__":
    getnerate_yolo_labels_set()