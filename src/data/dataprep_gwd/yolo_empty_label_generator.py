from pathlib import Path
from glob import glob
from tqdm import tqdm

def reconcile_label_differences(path_images: Path or str, path_labels: Path or str,):
    """
    Create all missing label files by checking the differences between available images against the available label files
    :param path_images:
    :param path_labels:
    :return:
    """
    path_images = Path(path_images)
    path_labels = Path(path_labels)

    # List all images as set
    path_absolute_images = glob(str(path_images / "*.jpg"), recursive=False)
    filestem_images = set(map(lambda full_path: Path(full_path).stem, path_absolute_images))

    # List all labels as set
    path_absolute_labels = glob(str(path_labels / "*.txt"), recursive=False)
    filestem_labels = set(map(lambda full_path: Path(full_path).stem, path_absolute_labels))

    # Find list of all missing images.
    missing_images = filestem_images.difference(filestem_labels)

    # Create all missing images:
    for image in tqdm(missing_images):
        (Path(path_labels) / f"{image}.txt").touch()

if __name__ == '__main__':
    reconcile_label_differences(
        "/home/dyt811/Git/cvnnig/data_gwd_yolo/images/train",
        "/home/dyt811/Git/cvnnig/data_gwd_yolo/labels/train"
    )