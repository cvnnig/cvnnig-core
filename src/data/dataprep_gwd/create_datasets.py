import click
from src.data.data_frame import (load_random_partial_subsets,
                                 load_train_processed_csv,
                                 split_dataframe_via_list,
                                 save_to_feather)

@click.command()
@click.option('--threshold', default=0.8, help='split percentage of training and validation set')
@click.option('--sample', default=False, help='create a sample training and validation set')
def create_datasets(threshold, sample):

    # Load processed data frames.
    df_train_processed = load_train_processed_csv(sample)

    # Get list of unique file names.
    unique_files = df_train_processed.file_name.unique()

    # Get a random list of training files, no replacement, ensure that it is a SET and no repeat.
    train_files = load_random_partial_subsets(unique_files, threshold=threshold)
    
    # split data
    (df_train_partial, df_val_partial) = split_dataframe_via_list(df_train_processed, 
                                                                  "file_name", 
                                                                  list(train_files))
    
    save_to_feather(df_train_partial, df_val_partial, threshold=threshold, sample=sample)
    
if __name__ == '__main__':
    create_datasets()