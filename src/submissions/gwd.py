from src.models_cvnnig.gwd.yolo_model_restore import GWDRestoration
from src.models_cvnnig.gwd.yolo_model_predict import GWDPrediction
from src.models_cvnnig.template.KaggleMixin import KagglePackingMixin
from src.envpath import AllPaths
from src.modeling.arch.yolov5.mc_adaptation.BasisConfiguration import YOLOConfig
from pathlib import Path
path_test_data = AllPaths.test_data

class GWDSubmission(GWDRestoration, GWDPrediction, KagglePackingMixin):
    """
    This is the submission class to be instantiated with the right information on Kaggle notebook in order to produce a submission.csv.
    """

    def __init__(self,
                 input_path_test: Path or str,
                 input_path_model_weight: Path or str
                 ):
        """
        For kaggle project submission, must ensure to reference the YAML node which was trained on.
        :param input_CfgNode:
        """
        self.path_test = Path(input_path_test)
        self.path_weight = Path(input_path_model_weight)
        self.config: YOLOConfig = self.configure()

if __name__=="__main__":
    # Instantiate class
    submission = GWDSubmission(input_path_model_weight="/home/dyt811/Git/cvnnig/src/modeling/arch/yolov5/runs/exp13/weights/last.pt",
                               input_path_test="/home/dyt811/Git/cvnnig/data_gwd/raw/test")

    # Load default (using .env)
    submission.configure()
    submission.eval(path_out="/tmp/test_out")

    # Generated output.
    submission.generate_kaggle_upload_zip(path_out="/tmp/test_out_data")

    # Secondary steps to aggregate the prediction stpes.
    submission.parse(path_out="/tmp/test_out")