
import pytest
import torch
from darknet import parse_cfg, create_modules, DarkNet, get_test_input
from envpath import AllPaths
from log import logger
import pytest

@pytest.fixture
def get_cfg():
    return AllPaths.config / "yolov3.cfg"

def test_parse_cfg(get_cfg):
    logger.info(parse_cfg(get_cfg))

def test_create_modules(get_cfg):
    blocks = parse_cfg(get_cfg)
    logger.info(create_modules(blocks))

def test_prediction(get_cfg):
    model = DarkNet("cfg/yolov3.cfg")
    model.load_weights("yolov3.weights")
    inp = get_test_input()
    pred = model(inp, torch.cuda.is_available())
    print(pred)
    # This should have a 10647 x 85 table, each ROW of the 10647 is a bounding box. (4 bbox + 1 objectness score + 80 class scores)
    # Unit test output is actually only 8112 vs 10647 though? odd...
    # 10647 is the resutl of bbox predicted per image.