import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
import numpy as np
import cv2
from torch import Tensor
from typing import List, Tuple
from log import logger

def predict_transform(prediction: Tensor,
                      inp_dim: int,
                      anchors: List[Tuple[int, int]],
                      num_classes: int,
                      CUDA: bool = True):
    """
    Take detection feature map
    Turn into 2D tensor
    where Row: attribute of a BBOX

    Bbox in order of:
    1st,2nd,3rd.. BBox at 0,0
    1st,2nd,3rd.. BBox at 0,1
    ...
    1st,2nd,3rd.. BBox at 7,0
    1st,2nd,3rd.. BBox at 7,1
    ...

    :param prediction: Detection Feature Map.
    :param inp_dim: Input image dimension
    :param anchors: Example, [(10,13),  (16,30),  (33,23)]
    :param num_classes:
    :param CUDA:
    :return:
    """

    # number of bbox predictions?
    batch_size = prediction.size(0)

    # Stride rounded... to the smallest int?
    stride: int = inp_dim // prediction.size(2)

    # Feature Map Gride Size? grid_size x grid_size
    grid_size: int = inp_dim // stride

    # Each bbox has xywh objectness + class of objects
    bbox_attrs: int = 5 + num_classes

    # Number of Tuple in the List of Tuple of (int, int)
    num_anchors: int = len(anchors)

    # Reshape it into a 2-D tensor, where each row of the tensor corresponds to attributes of a bounding box
    prediction = prediction.view(batch_size, bbox_attrs * num_anchors, grid_size * grid_size)
    # Reshape???
    # Ensure index alignment. todo: Check 2x3 vs 1x6 toy example
    prediction = prediction.transpose(1, 2).contiguous()
    # The final view used for indexing:
    prediction = prediction.view(batch_size,
                                 grid_size * grid_size * num_anchors,
                                 bbox_attrs)
    # Attrs order: cetner-x,center-y,

    # Convert anchors to tuple, after dividing by the Stride.
    anchors = [(a[0] / stride, a[1] / stride) for a in anchors]

    # Center_X
    prediction[:, :, 0] = torch.sigmoid(prediction[:, :, 0])

    # Center_Y
    prediction[:, :, 1] = torch.sigmoid(prediction[:, :, 1])

    # 2: w in proportion to modify th BBOX
    # 3: h in proportion to modify the BBOX

    # Object score.
    prediction[:, :, 4] = torch.sigmoid(prediction[:, :, 4])

    # Center offset?
    grid = np.arange(grid_size)
    a, b = np.meshgrid(grid, grid)

    x_offset = torch.FloatTensor(a).view(-1, 1)
    y_offset = torch.FloatTensor(b).view(-1, 1)

    if CUDA:
        x_offset = x_offset.cuda()
        y_offset = y_offset.cuda()

    # Calculate the Center coordinate grid offset.
    # ???
    x_y_offset = torch.cat((x_offset, y_offset), 1).repeat(1, num_anchors).view(-1, 2).unsqueeze(0)

    if CUDA:
        x_y_offset = x_y_offset.cuda()

    # both 0 and 1: Center X AND Center Y.
    prediction[:, :, :2] += x_y_offset


    # Apply the ratio modification tot he dimensions of the BBOX
    anchors = torch.FloatTensor(anchors)
    if CUDA:
        anchors = anchors.cuda()
    anchors = anchors.repeat(grid_size * grid_size, 1).unsqueeze(0)

    # 2=width, 3=height
    prediction[:, :, 2:4] = torch.exp(prediction[:, :, 2:4]) * anchors

    # Sigmoid activation for the class score
    prediction[:, :, 5: 5+num_classes] = torch.sigmoid((prediction[:, :, 5:5 + num_classes]))

    # Resize detection feature map to the size of the input image.
    prediction[:, :, :4] *= stride

    return prediction

def unique(tensor: Tensor) -> Tensor:
    """
    Get all classes present in an image.
    :param tensor:
    :return:
    """
    # Convert input tenosr to CPU and numpy.
    tensor_np = tensor.cpu().numpy()
    # Call numpy unique function on the tensor.
    unique_np = np.unique(tensor_np)
    # Convert it back to TORCH tensor.
    unique_tensor = torch.from_numpy(unique_np)

    # Create a new Tensor from the shape of the unique_tensor
    tensor_res = tensor.new(unique_tensor.shape)

    # Copy the values over. >>"in place"<<
    tensor_res.copy_(unique_tensor)
    return tensor_res

def bbox_iou(box1, box2) -> float:
    """
    Given any two bbox with min/max x/y, it retrunt eh IOU
    :param box1:
    :param box2:
    :return:
    """
    # Get the coordinates fo the bounging boxes
    # XMin, YMIn, XMax, YMax
    b1_x1, b1_y1, b1_x2,b1_y2 = box1[:, 0], box1[:, 1], box1[:, 2], box1[:, 3]
    b2_x1, b2_y1, b2_x2,b2_y2 = box2[:, 0], box2[:, 1], box2[:, 2], box2[:, 3]

    # Get the coordinates fo the INTERSECTION RECTANGLE
    # Max X
    inter_rect_x1 = torch.max(b1_x1, b2_x1)
    # Max Y
    inter_rect_y1 = torch.max(b1_y1, b2_y1)
    # Min X
    inter_rect_x2 = torch.min(b1_x2, b2_x2)
    # Min Y
    inter_rect_y2 = torch.min(b1_y2, b2_y2)

    # Intersection area
    # Why +1? Also, why... min - max?
    inter_area = torch.clamp(inter_rect_x2 - inter_rect_x1 + 1, min=0) * \
                 torch.clamp(inter_rect_y2 - inter_rect_y1 + 1, min=0)
    # why +1????
    # Union Area
    # B1 width x B1 height
    # why +1????
    b1_area = (b1_x2 - b1_x1 + 1) * (b1_y2 - b1_y1 + 1)
    # B2 width x B2 height
    # why +1????
    b2_area = (b2_x2 - b2_x1 + 1) * (b2_y2 - b2_y1 + 1)

    iou = inter_area / (b1_area+b2_area - inter_area)
    return iou

def write_results(prediction: Tensor, confidence: float, num_classes: int, nms_conf: float = 0.4):
    # Generate a Tensor where confidence score greater than the threshold, convert to float and
    conf_mask: Tensor = (prediction[:, :, 4] > confidence).float().unsqueeze(2)
    prediction = prediction * conf_mask

    # Convert results from the CenterX, CenterY, Height, Width to TopLeftX, TopLeftY, RightBottomX, RightBottomY (minXY,maxXY)
    box_corner: Tensor = prediction.new(prediction.shape)

    # MinX , Center X - 50% Width
    box_corner[:, :, 0] = (prediction[:, :, 0] - prediction[:, :, 2] / 2)

    # MinY, Center Y - 50% Height
    box_corner[:, :, 1] = (prediction[:, :, 1] - prediction[:, :, 3] / 2)

    # MaxX, Center X + 50% Width
    box_corner[:, :, 2] = (prediction[:, :, 0] + prediction[:, :, 2] / 2)
    # MaxY, Center Y + 50% Height
    box_corner[:, :, 3] = (prediction[:, :, 1] + prediction[:, :, 3] / 2)

    # Convert all four box_corner vector to prediction vector.
    prediction[:, :, :4] = box_corner[:, :, :4]

    # True detection per image VARIES and NMS and Confidence thresholding has to be done at per image level.

    # Must loop over prediction batches o
    batch_size: int = prediction.size(0)

    write: bool = False  # used to indicate that output tensor has not yet been initialized.

    # Iterate through each IMAGE.
    for index_batch in range(batch_size):

        # Get all the boxes predicted by that image?
        image_pred: Tensor = prediction[index_batch]

        # Objectness Thresholding: we only care about 5th to 85 which are the classes objectness vector
        max_conf, max_conf_score = torch.max(
            image_pred[:, 5:5+num_classes],
            1
        )  # why 1? OH. threshold max at 1... even if somehow predicted 1?

        # Expand the dimension by 1.
        max_conf: Tensor = max_conf.float().unsqueeze(1)

        max_conf_score: Tensor = max_conf_score.float().unsqueeze(1)

        # ???
        seq = (image_pred[:, :5], max_conf, max_conf_score)
        image_pred = torch.cat(seq, 1)

        # Non zero mask.
        non_zero_indicator_masks = (torch.nonzero(image_pred[:, 4]))
        try:
            # image_pred must be contiguous?
            image_pred_ = image_pred[non_zero_indicator_masks.squeeze(), :].view(-1, 7)
        except:
            logger.warn("Most likely, nothing got detected.")
            logger.warn("Something went wrong while trying to view the non-zero mask. Maybe try contiguous?")
            continue

        # lower PyTorch 0.4 compatibility?
        if image_pred_.shape[0] == 0:
            continue

        # CLass Detection in the image:
        img_classes = unique(image_pred_[:, -1])  # -1 index holds the class ind

        # NMS
        # Go Through EACH class trained.
        for index_class in img_classes:

            # Get the detection of one class ???
            # expand vecotor at dimension 1?
            class_mask = image_pred_ * (image_pred_[:, -1] == index_class).float().unsqueeze(1)

            # Make a mask/indicator tensor for all non-zero.
            class_mask_indicator = torch.nonzero(class_mask[:, -2]).squeeze()
            # ???
            image_pred_class = image_pred_[class_mask_indicator].view(-1, 7)  # why 7?

            # Sort the detection such that max objectness confidence is at the top.
            index_sorted_by_descneding_objectness_score = torch.sort(image_pred_class[:, 4], descending=True)[1]

            ### ???
            image_pred_class = image_pred_class[index_sorted_by_descneding_objectness_score]
            index_batch = image_pred_class.size(0)  # Get the number of detections.

            # GO through each index of detection.
            for index_detection in range(index_batch):
                # Get the IOUs of all the boxes that come after the one we are looking in the loop
                try:
                    # 1st indexed by variable i
                    # 2nd indexed by tensor of multiple rows of bbox.
                    # return is a tensor containing IOUs of the bbox of FIRST PARAMETER nad the rest .
                    # i.e. we compare bbox 1 with bbox2, 3, 4,... .and then bbox2, with 3,4,5,6...
                    ious = bbox_iou(
                        image_pred_class[index_detection].unsqueeze(0),  # first bbox.  IoU of box, indexed by i
                        image_pred_class[index_detection+1:]  # list of bbox? all the bounding boxes having indices higher than i
                    )

                except ValueError:
                    break

                # When reaching the end of the range.
                except IndexError:
                    break

                # create a mask where it zero out all the detection that have
                # 1 for all regions less than the threshold.

                # Zero out all the deteciton that have IOU > threshold???
                iou_mask = (ious < nms_conf).float().unsqueeze(1)

                # This will multiply with the mask... and all 0 areas are limnated.
                image_pred_class[index_detection+1:] *= iou_mask

                # Remove the non-zero entries:
                non_zero_indicator_masks = torch.nonzero(image_pred_class[:, 4]).squeeze()
                #
                image_pred_class = image_pred_class[non_zero_indicator_masks].view(-1, 7)

            batch_indictor = image_pred_class.new(image_pred_class.size(0), 1).fill_(index_batch)
            # Repeat the batch_id for as many detection of the class cls in the iamge?
            seq = batch_indictor, image_pred_class

            # Only initialize when needed.
            if not write:
                output = torch.cat(seq, 1)
                write = True
            else:
                out = torch.cat(seq, 1)
                output = torch.cat((output, out))

    try:
        return output
    except:
        # Absolute 0 detection case.
        return 0
