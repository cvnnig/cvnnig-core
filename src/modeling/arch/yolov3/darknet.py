# Underlying architecture of YOLO
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
from layers import layer_yolo_detection, layer_shortcut, layer_route, layer_upsample, \
    layer_convolution
from log import logger
from pathlib import Path
from keywords import *
from typing import Tuple, List
import torch
from util import predict_transform
from torch import Tensor
import numpy as np
import cv2

def parse_cfg(cfgfile: Path):
    """
    Parse the Configuration file into blocks: a list of dict objects.
    :param cfgfile:
    :return:
    """
    # Parse the CFG to build the architecture
    # return list of blocks. Dictionary in a list.
    file = open(cfgfile, 'r')
    lines = file.read().split('\n')
    lines = [x for x in lines if len(x) > 0]  # filter empty lines
    lines = [x for x in lines if x[0] != '#']  # filter comments... but what about beyond position 0?
    lines = [x.rstrip().lstrip() for x in lines]  # remove edge white spaces.

    block: dict = {}
    blocks: list = []
    for line in lines:
        if line[0] == "[": # handle block?
            if len(block) != 0:
                blocks.append(block)  # finish the previous block by appending to the LIST, start a new block.
                block = {}
            block[KW_TYPE] = line[1:-1].rstrip()  # remove brackets, get block type name.
        else:  # handle key value pair.
            key, value = line.split("=")  # get key value pairs, start appending them to the block.
            block[key.rstrip()] = value.lstrip()  # remove value's left white space, remove key's right white space.
    blocks.append(block)
    logger.debug(blocks)
    return blocks


def create_modules(blocks: List[dict]) -> Tuple[dict, nn.ModuleList]:
    """
    Create  the nn.ModuleList from the List of Dicts
    :param blocks:
    :return:
    """
    net_info = blocks[0]  # Capture information about the network IO configuration.
    module_list: nn.ModuleList = nn.ModuleList()
    prev_filters = 3  # First layer RGB
    output_filters: list = [] # where the various OUTPUT/feature map are concatenated and tracked

    # Process each block
    for index, dict_block in enumerate(blocks[1:]):

        module = nn.Sequential()
        # Check the type of block

        # Convolutional blocks.
        if dict_block[KW_TYPE] == KW_CONVOLUTIONAL:
            filters = layer_convolution(dict_block, index, module, prev_filters)

        # Upsample blocks.
        elif dict_block[KW_TYPE] == KW_UPSAMPLE:
            layer_upsample(dict_block, index, module)

        # Route blocks
        elif dict_block[KW_TYPE] == KW_ROUTE:
            filters = layer_route(dict_block, filters, index, module, output_filters)

        # Shortcut blocks.
        elif dict_block[KW_TYPE] == KW_SHORTCUT:
            layer_shortcut(index, module)

        # Yolo Detection blocks
        elif dict_block[KW_TYPE] == KW_YOLO:
            layer_yolo_detection(dict_block, index, module)
        else:
            logger.error("Unanticipated Layers!")
        # Create a new module for the tyep of block

        # Append to the module_list
        module_list.append(module)

        # These are ONLY updated... if a ROUTE or CONV block.
        prev_filters = filters
        output_filters.append(filters)

    return (net_info, module_list)

class DarkNet(nn.Module):
    def __init__(self, cfgfile: Path):
        super(DarkNet, self).__init__()

        # blocks[0] is the net information.
        self.blocks = parse_cfg(cfgfile)

        # self.module_list is created from blocks[1] onward
        self.net_info, self.module_list = create_modules(self.blocks)

    def forward(self, detection_feature_tensor: Tensor, CUDA: bool):
        modules = self.blocks[1:]

        # cache output for the route layer
        # The keys are the the indices of the layers, and the values are the feature maps
        # ince route and shortcut layers need output maps from previous layers, we cache the output feature maps of every layer in a dict outputs
        outputs: dict = {}

        # Flag to indicate the Tensor/Collector/TesnorFeatureMap has not bee initialized
        write = 0

        #Iterate through the modules based on index
        for index, module in enumerate(modules):
            index: int
            module: dict

            # Retrieve module type.
            module_type: str = module[KW_TYPE]

            # Handle convolution/upsample layer, via simple propagation
            if module_type == KW_CONVOLUTIONAL or module_type == KW_UPSAMPLE:
                detection_feature_tensor = self.module_list[index](detection_feature_tensor)

            # Handle route layer
            elif module_type == KW_ROUTE:
                # Purpose of Route: bring previous layers and/or concatenation

                # Get layers attribute (all the way from config file)
                layers: str = module[KW_LAYERS]
                layers: List[int] = [int(a) for a in layers]

                route_first: int = layers[0]  # relative position index
                # Calculate the position offset
                route_first_relative: int = self.convert_route_relative(index, route_first)


                if len(layers) == 1:
                    detection_feature_tensor = outputs[index + route_first_relative]   # this must be absolute position index.

                # SKip + Concatenate case
                else:
                    route_second: int = layers[1]
                    route_second_relative = self.convert_route_relative(index, route_second)

                    map1 = outputs[index + route_first_relative]   # tensor thickness does not change.
                    map2 = outputs[index + route_second_relative]   # tensor thickness does not change change.

                    # Concatenate the two layers
                    detection_feature_tensor = torch.cat((map1, map2), 1)  # why 1? Because depth/Channel wise concatenation

                    # Torch convention:
                    # Batch, Channel, Height, Width,

            elif module_type == KW_SHORTCUT:
                from_ = int(module[KW_FROM])
                detection_feature_tensor = outputs[index - 1] + outputs[index + from_]

            # The feature pyramid modules:
            elif module_type == "yolo":

                # Get the anchors specified in the YOLO config file.
                # Zero here because all three YOLO blocks used the same anchors.
                # List of tuples?
                anchors: List[Tuple[int, int]] = self.module_list[index][0].anchors

                # Get the input dimensions, assumed to be square here!
                inp_dim: int = int(self.net_info[KW_HEIGHT])

                # Get the number of classes
                num_classes: int = int(module["classes"])

                # Transform


                detection_feature_tensor = detection_feature_tensor.data
                if CUDA:
                    detection_feature_tensor = detection_feature_tensor.cuda()
                detection_feature_tensor = predict_transform(
                    detection_feature_tensor,
                    inp_dim,
                    anchors,
                    num_classes,
                    CUDA
                )

                # initialize the detections output at the FIRST TIME.
                if not write:
                    detections = detection_feature_tensor
                    write = 1
                else:
                    detections = torch.cat((detections, detection_feature_tensor), 1)

            outputs[index] = detection_feature_tensor
            logger.info(f"Output {index}, {detection_feature_tensor.shape}")

        return detections

    def convert_route_relative(self, index_current_layer, input_route_number_reference):

        # positive annotation means ABSOLUTE layer number reference
        if input_route_number_reference > 0:
            route_number_reference_relative = input_route_number_reference - index_current_layer

        # negative number means RELATIVE layer number reference
        else:
            route_number_reference_relative = input_route_number_reference

        return route_number_reference_relative

    def load_weights(self, path_weight_file: Path):
        """
        Load the weight file from path givne
        :param weightfile:
        :return:
        """
        with open(path_weight_file, "rb") as fp:
            # The first five values are header information
            # Major Version, Minor Version, Subversion, 4/5 IMage seen by the network during training
            header = np.fromfile(fp, dtype=np.int32, count=5)
            self.header = torch.from_numpy(header)
            self.seen = self.header[3]

            # weights stored as float32
            weights = np.fromfile(fp, dtype=np.float32)
            ptr = 0
            for i in range(len(self.module_list)):
                module_type = self.blocks[i + 1]["type"]

                # if module type is convolutional load weights, otherwise ignore:

                if module_type == "convolutional":
                    model = self.module_list[i]
                    try:
                        batch_normalize = int(self.blocks[i+1]["batch_normalize"])
                    except:
                        batch_normalize = 0

                    conv = model[0]

                    if batch_normalize:
                        bn = model[1]

                        # Get the number of weights of batch normalization layer
                        num_bn_biases = bn.bias.numel()

                        # Load the weights:
                        bn_biases = torch.from_numpy(weights[ptr:ptr + num_bn_biases])
                        ptr += num_bn_biases

                        bn_weights = torch.from_numpy(weights[ptr:ptr+num_bn_biases])
                        ptr += num_bn_biases

                        bn_running_mean = torch.from_numpy(weights[ptr:ptr+num_bn_biases])
                        ptr += num_bn_biases

                        bn_running_var = torch.from_numpy(weights[ptr:ptr+num_bn_biases])
                        ptr += num_bn_biases

                        # Case the loaded weights into dims of model weights:
                        bn_biases = bn_biases.view_as(bn.bias.data)
                        bn_weights = bn_weights.view_as(bn.weight.data)
                        bn_running_mean = bn_running_mean.view_as(bn.running_mean)
                        bn_running_var = bn_running_var.view_as(bn.running_var)

                        bn.bias.data.copy_(bn_biases)
                        bn.weight.data.copy_(bn_weights)
                        bn.running_mean.copy_(bn_running_mean)
                        bn.running_var.copy_(bn_running_var)
                    else:
                        # Number of biases
                        num_biases = conv.bias.numel()

                        # Load the weights:
                        conv_biases = torch.from_numpy(weights[ptr:ptr+num_biases])
                        ptr = ptr + num_biases

                        # reshape the loaded weights according to the dims of the model weight
                        conv_biases = conv_biases.view_as(conv.bias.data)

                        # Finally copy the data
                        conv.bias.data.copy_(conv_biases)

                    # lets load the weight for the convolutional layers
                    num_weights = conv.weight.numel()

                    conv_weights = torch.from_numpy(weights[ptr:ptr+num_weights])
                    ptr = ptr + num_weights

                    conv_weights = conv_weights.view_as(conv.weight.data)
                    conv.weight.data.copy_(conv_weights)

def get_test_input():
    img = cv2.imread("dog-cycle-car.png")

    # Resize to input dimenion
    img = cv2.resize(img, (416, 416))

    img_ = img[:, :, ::-1].transpose((2, 0, 1))  # BGR > RGB, H X W C -> C X H X W

    # Add 0 to batch channel.
    img_ = img_[np.newaxis, :, :, :]/255.0

    # Convert to float
    img_ = torch.from_numpy(img_).float()

    # Convert to variable.
    img_ = Variable(img_)
    return img_

if __name__ == "__main__":
    model = DarkNet("cfg/yolov3.cfg")
    model.load_weights("yolov3.weights")
    inp = get_test_input()
    pred = model(inp, torch.cuda.is_available())
    print(pred)