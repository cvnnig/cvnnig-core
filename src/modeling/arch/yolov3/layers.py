from typing import List, Tuple

from torch import nn as nn

from keywords import KW_MASK, KW_ANCHORS, KW_LAYERS, KW_STRIDE, KW_BILINEAR, KW_ACTIVATION, KW_BATCH_NORMALIZE, \
    KW_FILTERS, KW_PAD, KW_SIZE, KW_LEAKY


class EmptyLayer(nn.Module):
    def __init__(self):
        super(EmptyLayer, self).__init__()


class DetectionLayer(nn.Module):
    def __init__(self, anchors):
        super(DetectionLayer, self).__init__()
        self.anchors = anchors


def layer_yolo_detection(dict_block, index, module):
    # The output of YOLO is a convolutional feature map that contains the bounding box attributes along the depth of the feature map.
    
    # Split into list of int, indicating the INDEX of masks used.
    mask = dict_block[KW_MASK].split(",")
    mask: List[int] = [int(x) for x in mask]

    # split into list of ints, indicating the W/H ratio of the mask used.
    anchors: List[str] = dict_block[KW_ANCHORS].split(",")
    anchors: List[int] = [int(a) for a in anchors]
    # group them by two.
    anchors: List[Tuple[int, int]] = [
        (anchors[i], anchors[i + 1]) for i in range(0, len(anchors), 2)
    ]
    # Actual used mask, as specified by mask dict entry:
    anchors: List[Tuple[int, int]] = [anchors[i] for i in mask]

    detection = DetectionLayer(anchors)

    module.add_module(f"Detection_{index}", detection)


def layer_shortcut(index, module):
    shortcut = EmptyLayer()
    module.add_module(f"shortcut_{index}", shortcut)


def layer_route(dict_block, filters, index, module, output_filters):
    # Parse the layer index reference to Start vs End
    dict_block[KW_LAYERS] = dict_block[KW_LAYERS].split(',')
    # start of a route, it must have at least 1 values.
    start = int(dict_block[KW_LAYERS][0])
    # end, if there is one:
    try:
        end = int(dict_block[KW_LAYERS][1])
    except:
        end = 0
    # Positive Annotation???:
    # Calculate the relative layer number to current one.
    # Looking AHEAD???
    # current index = 8, start = 5,
    if start > 0:
        start = start - index
    if end > 0:
        end = end - index
    route = EmptyLayer()
    module.add_module(f"route_{index}", route)
    if end < 0:
        # if we are concatenating maps.
        filters = output_filters[index + start] + output_filters[index + end]
    else:
        filters = output_filters[index + start]
    return filters


def layer_upsample(dict_block, index, module):
    stride: int = int(dict_block[KW_STRIDE])
    upsample = nn.Upsample(scale_factor=2, mode=KW_BILINEAR)
    module.add_module(f"upsample_{index}", upsample)


def layer_convolution(dict_block, index, module, prev_filters):
    # Get information about the layer
    activation = dict_block[KW_ACTIVATION]
    try:
        batch_normalize = int(dict_block[KW_BATCH_NORMALIZE])
        bias = False
    except:
        batch_normalize = 0
        bias = True
    filters = int(dict_block[KW_FILTERS])
    padding = int(dict_block[KW_PAD])
    kernel_size = int(dict_block[KW_SIZE])
    stride = int(dict_block[KW_STRIDE])
    if padding:
        pad = (kernel_size - 1) // 2
    else:
        pad = 0
    # Add the convolutional layer:
    layer_conv2d = nn.Conv2d(
        in_channels=prev_filters,
        out_channels=filters,
        kernel_size=kernel_size,
        stride=stride,
        padding=pad,
        bias=bias)
    module.add_module(f"conv_{index}", layer_conv2d)
    if batch_normalize:
        bn = nn.BatchNorm2d(filters)
        module.add_module(f"batch_normalize_{index}", bn)
    # Check the activation
    # Either linear or LeakyReLU
    if activation == KW_LEAKY:
        activn = nn.LeakyReLU(0.1, inplace=True)
        module.add_module(f"leaky_{index}", activn)
    return filters