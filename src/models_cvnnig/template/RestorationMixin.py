from abc import abstractmethod, ABC
from pathlib import Path

class RestorationMixin(ABC):

    @abstractmethod
    def restore(self, path_weight: Path or str):
        raise NotImplementedError("Must implement restoration method.")

    @abstractmethod
    def configure(self, path_weight: Path or str):
        raise NotImplementedError("Must implement configuration method.")
