from abc import abstractmethod, ABC
from pathlib import Path
from pandas import DataFrame

class PredictionMixin(ABC):

    @abstractmethod
    def test_eval(self, path_test_data: Path or str) -> DataFrame:
        """
        Test the prediciton process.
        :param path_test_data:
        :return:
        """
        raise NotImplementedError("Must implement test evaluation method before calling it.")

    @abstractmethod
    def eval(self, path_data: Path or str) -> DataFrame:
        """
        Test the prediciton process.
        :param path_test_data:
        :return:
        """
        raise NotImplementedError("Must implement evaluation method before calling it.")