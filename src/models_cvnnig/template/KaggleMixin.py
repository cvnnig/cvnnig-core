import zipfile
from pathlib import Path
from shutil import ignore_patterns, copytree
from abc import ABC
from src.data.zip import zipdir
from src.envpath import AllPaths

class KagglePackingMixin (ABC):
    path_test: Path
    path_weight: Path
    def generate_kaggle_upload(self, path_out: Path):
        """
        This ensure only the absolutely necessary data are copied over to a temporary location, ignoring everything.

        This generate a folder list and past on to be ignored by the copytree operations

        :return:
        """
        pattern_exclusion_files = ignore_patterns("*.p",
                                                  "*.md",
                                                  "*.pkl",
                                                  "*.zip",
                                                  "wheelhouse",
                                                  "train*.parquet",
                                                  "*.ipynb",
                                                  ".pytest*",
                                                  ".git",
                                                  "model_bak*.pt",
                                                  ".idea",
                                                  "reports",
                                                  "references",
                                                  "docs",
                                                  "2020-02*",
                                                  "src.egg-info",
                                                  "mlruns",
                                                  "test_*.py",
                                                  "*.md",
                                                  "submission.csv",
                                                  "detectron2",
                                                  "notebooks",
                                                  "*.jpg",
                                                  "*.txt",
                                                  "runs",
                                                  )

        # Go root path.
        path_root = Path(__file__).parents[3]

        copytree(path_root, path_out, ignore=pattern_exclusion_files)

    def generate_kaggle_upload_zip(self, path_out: str or Path = Path(r"/tmp/kaggle//")):
        path_out = Path(path_out)
        self.generate_kaggle_upload(path_out)
        # Source; https://stackoverflow.com/questions/1855095/how-to-create-a-zip-archive-of-a-directory-in-python

        # Generate zip file handle,
        zipf = zipfile.ZipFile(path_out.parent / 'Pipeline.zip', 'w', zipfile.ZIP_DEFLATED)

        # Call the zipping function using the zip handle.
        zipdir(path_out.absolute(), zipf)

        # Close zip file.
        zipf.close()