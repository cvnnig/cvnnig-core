import json
import os
import cv2
import random
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from pathlib import Path

# import some common detectron2 utilities
from detectron2.structures import BoxMode
from detectron2 import model_zoo
from detectron2.engine import DefaultPredictor
from detectron2.utils.visualizer import Visualizer
from detectron2.data import DatasetCatalog, MetadataCatalog
from detectron2.engine import DefaultTrainer
from detectron2.config import get_cfg

from detectron2.utils.visualizer import ColorMode

from detectron2_model_train import set_training_config, create_datadict, load_dataset_into_dataframes, register_datadict_for_detectron2

from datetime import datetime

from src.models_cvnnig.rle import rle_decode_string, rle2bbox
from environs import Env

env = Env()
env.read_env()

# Get training dataframe
path_data = Path(env("path_raw"))
path_image = path_data / "train/"
path_output = Path(env("path_output"))
import random
seed = random.randint(0, 99999999)

def get_pretrained_predictor(path_yaml: str, threshold: float = 0.5):
    """
    Get the pretrained predictor based on mask
    """
    # Generate instance segmentation predictor:
    cfg = get_cfg()
    # Merging model configs with default
    cfg.merge_from_file(model_zoo.get_config_file(path_yaml))
    # Set threshold
    cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = threshold  # set threshold for this model

    # Get weights from Instance segmentation Mask RCNN R 50 FPN model
    cfg.MODEL.WEIGHTS = model_zoo.get_checkpoint_url(path_yaml)
    # Set Predictor.
    predictor = DefaultPredictor(cfg)

    return predictor


def get_pretrained_instance_predictor():
    """
    Get the pretrained predictor based on mask
    """
    predictor = get_pretrained_predictor("COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml")
    return predictor


def get_pretrained_keypoints_predictor():
    """
    Get the pretrained predictor based on mask
    """
    predictor = get_pretrained_predictor("COCO-Keypoints/keypoint_rcnn_R_50_FPN_3x.yaml")
    return predictor


def inference(cfg):
    from detectron2.evaluation import COCOEvaluator, inference_on_dataset

    from detectron2.data import build_detection_test_loader

    evaluator = COCOEvaluator("balloon_val", cfg, False, output_dir="./output/")
    val_loader = build_detection_test_loader(cfg, "balloon_val")
    inference_on_dataset(trainer.model, val_loader, evaluator)


# another equivalent way is to use trainer.test
# Not fully working yet.


def get_final_predictor(cfg, test_set: str = 'sample_fashion_test'):
    #  Inference & evaluation using the trained model
    # First, we create a predictor using the model we just trained: In [27]:
    cfg.MODEL.WEIGHTS = os.path.join(cfg.OUTPUT_DIR, "model_final.pth")
    cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = 0.5  # set the testing threshold for this model
    cfg.DATASETS.TEST = (test_set,)  # must be a tuple
    predictor = DefaultPredictor(cfg)
    return predictor


def show_predicted_image(datadic_val_66k):
    """
    Show 3 predicted images from the Fashion Dict (make sure it is the test set!)
    :param dict_test:
    :return:
    """
    # Show different images at random
    rows, cols = 3, 3
    plt.figure(figsize=(20, 20))



    # Randomly Grab 9 samples, iterate through rows of them, convert to list of tuple.  :
    list_tuple = list(datadic_val_66k.sample(n=50, random_state=seed).iterrows())
    _, list_datadic = zip(*list_tuple)



    for i, d in enumerate(list_datadic):
        time_stamp = datetime.now().isoformat().replace(":", "")

        im = cv2.imread(d["ImageId"])
        im = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)

        # Run through predictor
        outputs = predictor(im)

        # Visualize
        v = Visualizer(im[:, :, ::-1],
                       metadata=fashion_metadata,
                       scale=0.8,
                       instance_mode=ColorMode.IMAGE_BW  # remove the colors of unsegmented pixels
                       )
        # Bring the data back to CPU before passing to Numpy to draw
        v = v.draw_instance_predictions(outputs["instances"].to("cpu"))
        plt.imshow(v.get_image()[:, :, ::-1])
        # Prefix:
        prefix = Path(cfg.OUTPUT_DIR).name
        plt.imsave(f"/home/dyt811/Pictures/{prefix}_{time_stamp}.png", v.get_image()[:, :, ::-1])


if __name__ == "__main__":
    data_full, df_attributes, df_categories = load_dataset_into_dataframes()

    datadic_full = create_datadict(data_full)
    # Arbitrary split in training / testing dataframes
    datadic_train_266k = datadic_full[:266721].copy()
    datadic_val_66k = datadic_full[-66680:].copy()

    # View image?
    # fashion_dict = get_fashion_dict(df_copy[:50])

    # Save metadata object for visualizations

    register_datadict_for_detectron2(datadic_train_266k, "sample_fashion_train")
    register_datadict_for_detectron2(datadic_val_66k, "sample_fashion_test")

    cfg = set_training_config("sample_fashion_train", "sample_fashion_test")
    # show_predictted_image

    for path_output in [
        "/media/dyt811/A4FCE3EEFCE3B8A6/Git/bengali.ai/data/raw/trained_15k",
        "/media/dyt811/A4FCE3EEFCE3B8A6/Git/bengali.ai/data/raw/trained_100k",
        "/media/dyt811/A4FCE3EEFCE3B8A6/Git/bengali.ai/data/raw/trained_200k",
        "/media/dyt811/A4FCE3EEFCE3B8A6/Git/bengali.ai/data/raw/trained_900k"
    ]:
        # Update weight output
        cfg.OUTPUT_DIR = str(path_output)

        # Set the test data dictionary from the registry.
        predictor = get_final_predictor(cfg, "sample_fashion_test")

        # Get the meta data
        fashion_metadata = MetadataCatalog.get("sample_fashion_test")

        show_predicted_image(datadic_val_66k)
