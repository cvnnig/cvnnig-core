from pathlib import Path
from src.models_cvnnig.template.RestorationMixin import RestorationMixin
from src.modeling.arch.yolov5.mc_adaptation.BasisConfiguration import YOLOConfig

class GWDRestoration(RestorationMixin):
    path_weight: Path
    path_test: Path

    def configure(self, path_weight=None, path_test=None)-> YOLOConfig:
        """
        Load the config and weights necessary for the model, based on class instantiation yet allow overwritting and modifying.
        :param input_config:
        :param input_weight:
        :return:
        """
        # MC Run Start:
        config_test = YOLOConfig()
        config_test.device = "0"
        config_test.source = self.path_test if path_test is None else path_test
        config_test.weights = self.path_weight if path_weight is None else path_weight
        config_test.conf_thres = 0.5

        return config_test

    def restore(self, path_weight: Path or str):
        pass
