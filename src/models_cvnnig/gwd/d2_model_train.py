import json
import os
import cv2
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from detectron2.evaluation import COCOEvaluator, inference_on_dataset
from detectron2.config import get_cfg
from detectron2.engine import DefaultPredictor, DefaultTrainer, default_argument_parser, default_setup, launch
from detectron2.data import build_detection_train_loader, build_detection_test_loader

from src.envpath import AllPaths
from src.data.dataprep_gwd.dataset_mapper import WheatDatasetMapper
from src.data.dataprep_gwd.register import register_datadict
from src.models_cvnnig.gwd.config import add_gwd_config
from src.models_cvnnig.gwd.config import setup_prediction

# Slightly patched COCOTrainer with additional output folder
class CocoTrainer(DefaultTrainer):
    
    @classmethod
    def build_train_loader(cls, cfg):
        return build_detection_train_loader(cfg, mapper=WheatDatasetMapper(cfg))
    
    @classmethod
    def build_test_loader(cls, cfg, dataset_name):
        return build_detection_test_loader(cfg, dataset_name, mapper=WheatDatasetMapper(cfg))

    @classmethod
    def build_evaluator(cls, cfg, dataset_name, output_folder=None):
        if output_folder is None:
            os.makedirs("coco_eval", exist_ok=True)
            output_folder = "coco_eval"
        return COCOEvaluator(dataset_name, cfg, False, output_folder)

def main(args):

    # Load the config file and the pre-trained model weights
    cfg = setup_prediction(args)

    # Load the correct dataframe based on MODEL.DEBUG input
    if cfg.MODEL.DEBUG == 'sample':
        df_train_name, df_val_name = 'wheat_train_sample', 'wheat_val_sample'
    elif cfg.MODEL.DEBUG == 'full':
        df_train_name, df_val_name = 'wheat_train_full', ''
    else:
        df_train_name, df_val_name = 'wheat_train', 'wheat_val'

    # Load dataframe(s)
    df_train = pd.read_feather(AllPaths.processed / df_train_name)
    register_datadict(df_train)

    if df_train_name != 'full':
        df_val = pd.read_feather(AllPaths.processed / df_val_name)
        register_datadict(df_val, train=False)

    trainer = CocoTrainer(cfg)
    trainer.resume_or_load(resume=False)

    return trainer.train()

if __name__ == '__main__':
    args = default_argument_parser().parse_args()
    args.config_file = '/home/julien/data-science/kaggle/global-wheat-detection/configs/exp01.yaml'
    print('Command Line Args', args)
    launch(
        main,
        args.num_gpus,
        num_machines=args.num_machines,
        machine_rank=args.machine_rank,
        dist_url=args.dist_url,
        args=(args,)
    )
