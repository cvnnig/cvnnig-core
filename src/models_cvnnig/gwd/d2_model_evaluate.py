"""# EVALUATE"""

print(cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST)

cfg.MODEL.WEIGHTS = os.path.join(cfg.OUTPUT_DIR, "model_final.pth")
cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = 0.85
# predictor = DefaultPredictor(cfg)

evaluator = COCOEvaluator("wheat_val", cfg, False, output_dir="./output/")
val_loader = build_detection_test_loader(cfg, "wheat_val")
inference_on_dataset(trainer.model, val_loader, evaluator)