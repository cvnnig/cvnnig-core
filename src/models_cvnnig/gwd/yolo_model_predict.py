from src.models_cvnnig.template.PredictionMixin import PredictionMixin
import sys
sys.path.append('../')
import torch
from pandas import DataFrame
from src.modeling.arch.yolov5.mc_adaptation.BasisConfiguration import YOLOConfig
from src.modeling.arch.yolov5.detect_mc import detect
from pathlib import Path
from typing import List
from glob import glob
from src.data.csv_export import write_list_dict

class GWDPrediction(PredictionMixin):
    """
    This is a MixIn class, provid class based method to regular models.
    To REUSE this function, make sure the model class inherit from this
    MUST NOT HAVE class level data and variables.
    """
    path_test: str or Path
    config: YOLOConfig
    def eval(self, path_test=None, path_out=None):
        """
        Evaluate images in the folder Generate a bunch of .TXT files to be parsed and
        :param path_image:
        :return:
        """

        # Default value to be the instantiated value.
        if path_test is None:
            path_test = self.path_test

        assert self.config is not None
        self.config.source = path_test

        # Only overwrite if there is a provided path out.
        if path_out is not None:
            self.config.out = path_out

        with torch.no_grad():
            detect(self.config)
        self.parse()


    def test_eval(self, path_test=None, path_out=None):

        # Default value to be the instantiated value.
        if path_test is None:
            path_test = self.path_test

        assert self.config is not None
        self.config.source = path_test

        # Only overwrite if there is a provided path out.
        if path_out is not None:
            self.config.out = path_out
        # try:
        # Prediction mode.
        with torch.no_grad():
            detect(self.config)
        # except Exception as e:
        #     print(e)

    def parse(self, path_out: str or Path =None):
        """
        This is executeed once the predictions are done, and various txt files have been produced.
        :param path_out:
        :return:
        """
        if path_out is None:
            path_out = self.config.out

        list_images = glob(str(Path(path_out) / "*.jpg"))
        dict_aggregated_prediction: List[dict] = []
        for file_image in list_images:
            file_prediction = file_image.replace(".jpg", ".txt")
            image_id = Path(file_image).stem

            # No prediction situation.
            if not Path(file_prediction).exists():
                dict_aggregated_prediction.append({
                    "image_id": image_id,
                    "PredictionString": ""
                })
                continue

            # Yes prediction situation
            with open(file_prediction, "r") as f:
                content = f.read().replace("\n", " ")

                dict_aggregated_prediction.append({
                    "image_id": image_id,
                    "PredictionString": content
                })

        write_list_dict(dict_aggregated_prediction, Path(path_out) / "Submission.csv")



