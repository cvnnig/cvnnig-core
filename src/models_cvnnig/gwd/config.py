from detectron2.config import CfgNode as CN
from detectron2.config import get_cfg
from detectron2.engine import default_setup
from detectron2.utils.logger import setup_logger
import detectron2.utils.comm as comm
from detectron2 import model_zoo

from src.envpath import AllPaths




def add_gwd_config(cfg: CN):
    # Load the config file and the pre-trained model weights
    cfg.merge_from_file(
        model_zoo.get_config_file(
            "COCO-Detection/retinanet_R_101_FPN_3x.yaml"
        )
    )
    cfg.MODEL.WEIGHTS = model_zoo.get_checkpoint_url(
        "COCO-Detection/retinanet_R_101_FPN_3x.yaml"
    )
    # print(cfg.dump())
    cfg.DATASETS.TRAIN = ("wheat_train",)
    cfg.DATASETS.TEST = ("wheat_test",)
    cfg.DATALOADER.NUM_WORKERS = 4
    cfg.SOLVER.IMS_PER_BATCH = 4
    cfg.SOLVER.BASE_LR = 0.0002
    cfg.SOLVER.WARMUP_ITERS = 1000
    cfg.SOLVER.MAX_ITER = 1500
    cfg.SOLVER.STEPS = (1000, 1500)
    cfg.SOLVER.GAMMA = 0.05
    cfg.MODEL.ROI_HEADS.BATCH_SIZE_PER_IMAGE = 64
    cfg.MODEL.ROI_HEADS.NUM_CLASSES = 1
    cfg.MODEL.RETINANET.NUM_CLASSES = 1
    cfg.TEST.EVAL_PERIOD = 500
    cfg.MODEL.DEBUG = ''
    return cfg


def initialize_gwd_config():
    """
    Cannot directly merge until intialize the imaterialist config properly in the first place.
    :return:
    """
    cfg = get_cfg()
    add_gwd_config(cfg)
    return cfg

def setup_prediction(args):
    """
    Setup up the cfg per the prediction requirement.
    Will use weight specified in the environmental variable.
    :param args:
    :return:
    """
    cfg = initialize_gwd_config()

    # Merge from pretrained or opts
    cfg.merge_from_file(args.config_file)
    cfg.merge_from_list(args.opts)

    if AllPaths.trained_weights == '':
        cfg = update_weights_outpath(cfg, AllPaths.trained_weights)

    cfg.freeze()
    default_setup(cfg, args)
    # Setup logger for "imaterialist" module
    setup_logger(output=cfg.OUTPUT_DIR, distributed_rank=comm.get_rank(), name="Global Wheat Detection")
    return cfg


def update_weights_outpath(cfg, weights_path):
    """
    Update these two attributes using environmental variable because the CFG past along was hard coded.
    :param cfg:
    :param weights_path:
    :return:
    """
    # Add the trained weights
    cfg.MODEL.WEIGHTS = weights_path
    cfg.OUTPUT_DIR = AllPaths.output

    return cfg
