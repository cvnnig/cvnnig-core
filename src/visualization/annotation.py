import cv2
import pandas as pd
from src.envpath import AllPaths

def bbox_annotate_image(df_annotations: pd.DataFrame, resize=True):
    """
    Draw bbox using XMIN YMIN XMAX YMAX
    :param df_annotations:
    :param resize:
    :return:
    """
    # Get file name.
    file_name = df_annotations.file_name.to_numpy()[0]

    # Convert images from BGR to RGB
    # Ensure to load FULL path.
    # fixme: this is currently drawing XMIN YMIN XMAX YMAX
    # Should be able to draw XYWH or XYHW etc.
    img = cv2.cvtColor(cv2.imread(AllPaths.train_data / file_name), cv2.COLOR_BGR2RGB)

    # Go through each annotation row.
    for i, a in df_annotations.iterrows():

        # Draw a rectangle, be careful if the option int
        cv2.rectangle(img,
                      (int(a.x_min), int(a.y_min)),
                      (int(a.x_max), int(a.y_max)),
                      (0, 255, 0), 2)

    if not resize:
        return img

    return cv2.resize(img, (384, 384), interpolation=cv2.INTER_AREA)
