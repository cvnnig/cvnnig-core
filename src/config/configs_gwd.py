from detectron2.config import CfgNode as CN
from detectron2 import model_zoo
from detectron2.config import get_cfg
from pathlib import Path
from environs import Env
from detectron2.engine import default_setup
import detectron2.utils.comm as comm
from detectron2.utils.logger import setup_logger
import os

env = Env()
env.read_env()

def add_wheat_config(cfg: CN):
    """
    Add config for wheat-detection
    """
    _C = cfg

    _C.merge_from_file(model_zoo.get_config_file(
        "COCO-Detection/retinanet_R_101_FPN_3x.yaml"))
    _C.MODEL.WEIGHTS = model_zoo.get_checkpoint_url(
        "COCO-Detection/retinanet_R_101_FPN_3x.yaml")


    _C.DATASETS.TRAIN = ('wheat_train',)
    _C.DATASETS.TEST = ('wheat_val',)
    _C.MODEL.RETINANET.NUM_CLASSES = 1  # 1 classes in wheat-detection
    _C.MODEL.ROI_HEADS.NUM_CLASSES = 1
    _C.MODEL.DEBUG = False
    # this should ALWAYS be left at 1 because it will double or more memory usage if higher.
    _C.DATALOADER.NUM_WORKERS = 1

    _C.SOLVER.IMS_PER_BATCH: 4


def initialize_wheat_config():
    """
    Cannot directly merge until intialize the wheat config properly in the first place.
    :return:
    """
    cfg = get_cfg()
    add_wheat_config(cfg)
    return cfg

def setup_prediction(args):
    """
    Setup up the cfg per the prediction requirement.
    Will use weight specified in the environmental variable.
    :param args:
    :return:
    """
    cfg = initialize_wheat_config()

    # Merge from pretrained or opts
    cfg.merge_from_file(args.config_file)
    cfg.merge_from_list(args.opts)

    if env('path_trained_weights') != '':
        cfg = update_weights_outpath(cfg, env("path_trained_weights"))

    cfg.freeze()
    default_setup(cfg, args)
    # Setup logger for "imaterialist" module
    setup_logger(output=cfg.OUTPUT_DIR, distributed_rank=comm.get_rank(), name="wheat")
    return cfg

def update_weights_outpath(cfg, weights_path):
    """
    Update these two attributes using environmental variable because the CFG past along was hard coded.
    :param cfg:
    :param weights_path:
    :return:
    """
    # Add the trained weights
    cfg.MODEL.WEIGHTS = weights_path
    cfg.OUTPUT_DIR = env("path_outputs")

    return cfg
