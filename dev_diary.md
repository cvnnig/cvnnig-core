

2020-08-04T181415EST 
So annoying that YOLOV5 requirement is NOT explicit enough. It causes extra install that are massive. 
For instance, latest torch and torch visions are actually not required. 
Never name modules generic like "models" or "model". 
Cause fun import problem. 

Kaggle error is as cryptic. 

2020-08-04T141708EST 
Time is running short. 
I still need to write the actual parser. This will take the list of files, then aggregate them into a single CSV. 

Okay, glad I was able to find some refactored shortcut that makes this part of the process easier. 

2020-08-03T234057EST 
So there are really TWO major components to the process: 1) generate the uploaded repo snapshot trimmed of all unnecessary data. 
Then, there is the part where I also test out the prediction and ensure that a list of txt files are generated and are working as intended. 
I did both today. I fully debugged the refactored offline prediction process and also the zip process. NOw we have both, we are very close to successful submissions. 
Still probably need a way to ensure that the weight file itself is properly uploaded. 

2020-08-03T222429EST 
Yeah, the predict/restoration is COMPLEX and I am not sure I am entirely on the right track in terms of the inhritance. 
I might as well give it a try and see. Best way to learn. 


2020-08-03T214039EST 
It is slowly coming back to me. OMG. It has been almost half a year. The code needs to be able to generate the submsision CSV offline first. 


2020-08-03T211830EST 
Almost submission deadline, yet I have not gotten my package ready. This is looking very bad. 
I continue refactored the detection class to be somewhat more ergonomic and humanly readable. 
I further tested out camera based detection which worked out reasonably well, even post refactoring. 

So to package everything up. I think I will need quite a few things: 

1) CVNNIG repo as a source. 
2) the trained weight as a repo. 
3) the original data? 

2020-07-25T171320EST 
Had to interrupt the training. Hoping to resume the MC training soon

2020-07-25T143254EST
Okay, thankfully I googled the issue: https://github.com/ultralytics/yolov5/issues/507
The latest V2 code broke old YAML configurations and must be using new ones. 

2020-07-25T124059EST, Yang 
I have successfully merged in both Detectron2 and YOLOV5 repoistories into the current folder. However, a significant amount of work is still required to bring it up to Kaggle for final deployment. 

The refactoring of the YOLOV5 repository was maddening exhausting, the old code base is INTENSELY annoying and convoluted. I am really really surprised that the old dev was able to keep things rolling... I can barely make heads and tails out of it and the lack of unit test means no way the owner would know what changed. 

Ouch. I think because i have been following close to the source code of the YOLO5 repo, the update to their repo resulted in not backward compatibility with older model I trained. Sick. 

Okay, going to do a quick retraining then. Also going to retrain it in CVNNIG context. 

Hmmm... not sure if that is entirely related as even retraining is causing issues. I suspect might be a dependencies issue. Going to force update Detectron2 conda environment to incorporate YOLOV5 stuff too. HOpefully this will be backward compatibile. 

 
